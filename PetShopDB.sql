USE [master]
GO
/****** Object:  Database [PetshopDB]    Script Date: 19/9/2021 00:14:14 ******/
CREATE DATABASE [PetshopDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'PetshopDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\PetshopDB.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'PetshopDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\PetshopDB_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [PetshopDB] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [PetshopDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [PetshopDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [PetshopDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [PetshopDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [PetshopDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [PetshopDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [PetshopDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [PetshopDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [PetshopDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [PetshopDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [PetshopDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [PetshopDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [PetshopDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [PetshopDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [PetshopDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [PetshopDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [PetshopDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [PetshopDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [PetshopDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [PetshopDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [PetshopDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [PetshopDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [PetshopDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [PetshopDB] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [PetshopDB] SET  MULTI_USER 
GO
ALTER DATABASE [PetshopDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [PetshopDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [PetshopDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [PetshopDB] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [PetshopDB] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [PetshopDB] SET QUERY_STORE = OFF
GO
USE [PetshopDB]
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
USE [PetshopDB]
GO
/****** Object:  User [EstudioExpress_sa]    Script Date: 19/9/2021 00:14:14 ******/
CREATE USER [EstudioExpress_sa] FOR LOGIN [EstudioExpress_sa] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [EstudioExpress_sa]
GO
/****** Object:  Table [dbo].[bitacora]    Script Date: 19/9/2021 00:14:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bitacora](
	[idBitacora] [int] IDENTITY(1,1) NOT NULL,
	[criticidad] [int] NOT NULL,
	[descripcion] [nvarchar](256) NOT NULL,
	[fecha] [datetime] NOT NULL,
	[funcionalidad] [nvarchar](45) NOT NULL,
	[Usuario_idUsuario] [int] NULL,
	[digitoVerificadorH] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_bitacora_idBitacora] PRIMARY KEY CLUSTERED 
(
	[idBitacora] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Categorias]    Script Date: 19/9/2021 00:14:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categorias](
	[codcategoria] [char](2) NOT NULL,
	[descategoria] [varchar](20) NULL,
 CONSTRAINT [PK_Categorias] PRIMARY KEY CLUSTERED 
(
	[codcategoria] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Detalle_venta]    Script Date: 19/9/2021 00:14:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Detalle_venta](
	[VEN_Codigo] [char](10) NOT NULL,
	[DV_Cantidad] [int] NULL,
	[DV_Precio] [decimal](10, 2) NULL,
	[DV_SubTotal] [decimal](10, 2) NULL,
	[codproducto] [char](4) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[detallefactura]    Script Date: 19/9/2021 00:14:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[detallefactura](
	[idFacturaDetalle] [int] IDENTITY(1,1) NOT NULL,
	[descripcion] [nvarchar](50) NOT NULL,
	[monto] [float] NOT NULL,
	[idFactura] [int] NOT NULL,
 CONSTRAINT [PK_detallefactura] PRIMARY KEY CLUSTERED 
(
	[idFacturaDetalle] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[digitoverificadorvertical]    Script Date: 19/9/2021 00:14:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[digitoverificadorvertical](
	[idDigitoVerificadorVertical] [int] IDENTITY(1,1) NOT NULL,
	[tabla] [nvarchar](45) NOT NULL,
	[digitoVerificador] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_digitoverificadorvertical_idDigitoVerificadorVertical] PRIMARY KEY CLUSTERED 
(
	[idDigitoVerificadorVertical] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[familia]    Script Date: 19/9/2021 00:14:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[familia](
	[idFamilia] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [nvarchar](256) NOT NULL,
	[habilitado] [int] NOT NULL,
 CONSTRAINT [PK_familia_idFamilia] PRIMARY KEY CLUSTERED 
(
	[idFamilia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[familiapatente]    Script Date: 19/9/2021 00:14:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[familiapatente](
	[idFamiliaPatente] [int] IDENTITY(1,1) NOT NULL,
	[Patente_idPatente] [int] NOT NULL,
	[Familia_idFamilia] [int] NOT NULL,
	[digitoVerificadorH] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_familiapatente_idFamiliaPatente] PRIMARY KEY CLUSTERED 
(
	[idFamiliaPatente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[familiausuario]    Script Date: 19/9/2021 00:14:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[familiausuario](
	[idFamiliaUsuario] [int] IDENTITY(1,1) NOT NULL,
	[Familia_idFamilia] [int] NOT NULL,
	[Usuario_idUsuario] [int] NOT NULL,
 CONSTRAINT [PK_familiausuario_idFamiliaUsuario] PRIMARY KEY CLUSTERED 
(
	[idFamiliaUsuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[patente]    Script Date: 19/9/2021 00:14:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[patente](
	[idPatente] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [nvarchar](45) NOT NULL,
 CONSTRAINT [PK_patente_idPatente] PRIMARY KEY CLUSTERED 
(
	[idPatente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[patenteusuario]    Script Date: 19/9/2021 00:14:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[patenteusuario](
	[idPatente] [int] IDENTITY(1,1) NOT NULL,
	[esPermisiva] [smallint] NOT NULL,
	[Patente_idPatente] [int] NOT NULL,
	[Usuario_idUsuario] [int] NOT NULL,
	[digitoVerificadorH] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_patenteusuario_idPatente] PRIMARY KEY CLUSTERED 
(
	[idPatente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Productos]    Script Date: 19/9/2021 00:14:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Productos](
	[codproducto] [char](4) NOT NULL,
	[desproducto] [varchar](40) NULL,
	[codcategoria] [char](2) NULL,
	[preproducto] [decimal](18, 2) NULL,
	[canproducto] [int] NULL,
	[imagen] [varchar](40) NULL,
 CONSTRAINT [PK_Productos] PRIMARY KEY CLUSTERED 
(
	[codproducto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[usuario]    Script Date: 19/9/2021 00:14:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[usuario](
	[idUsuario] [int] IDENTITY(1,1) NOT NULL,
	[nombreUsuario] [nvarchar](256) NOT NULL,
	[nombre] [nvarchar](45) NOT NULL,
	[apellido] [nvarchar](45) NOT NULL,
	[email] [nvarchar](45) NOT NULL,
	[contrasena] [nvarchar](256) NOT NULL,
	[habilitado] [int] NOT NULL,
	[cantidadDeIntentos] [int] NOT NULL,
	[digitoVerificadorH] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_usuario_idUsuario] PRIMARY KEY CLUSTERED 
(
	[idUsuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[usuariocurso]    Script Date: 19/9/2021 00:14:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[usuariocurso](
	[Curso_idCurso] [int] NOT NULL,
	[Usuario_idUsuario] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Venta]    Script Date: 19/9/2021 00:14:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Venta](
	[VEN_Codigo] [char](10) NOT NULL,
	[VEN_Fecha] [varchar](50) NULL,
	[VEN_SuTotal] [decimal](10, 2) NULL,
	[VEN_IGV] [decimal](10, 2) NULL,
	[VEN_Total] [decimal](10, 2) NULL,
	[VEN_Cliente] [varchar](50) NULL,
 CONSTRAINT [PK_VENTA_1] PRIMARY KEY CLUSTERED 
(
	[VEN_Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[bitacora] ON 

INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (2, 2, N'4TXB7mxNQ48Yep2crakneQ==', CAST(N'2021-11-07T01:28:49.000' AS DateTime), N'LOGIN', 3, N'C70E5B4F62C0182E145910D43F34A9D2')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (3, 2, N'4TXB7mxNQ48Yep2crakneQ==', CAST(N'2021-11-07T01:29:14.000' AS DateTime), N'LOGIN', 3, N'D86D097320985F311DD1BC7C458A3CA0')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (4, 2, N'4TXB7mxNQ48Yep2crakneQ==', CAST(N'2021-11-08T04:23:47.000' AS DateTime), N'LOGIN', 3, N'2BFB8ED85FE1A2217B4038EF4A394DC6')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (5, 2, N'4TXB7mxNQ48Yep2crakneQ==', CAST(N'2021-11-08T04:53:41.000' AS DateTime), N'LOGIN', 3, N'2FAA6A3EDF4D1A166FB7A9744A732D1A')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (6, 2, N'4TXB7mxNQ48Yep2crakneQ==', CAST(N'2021-11-08T05:15:43.000' AS DateTime), N'LOGIN', 3, N'65DB089840D8136665B0D608FB859F37')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (7, 2, N'4TXB7mxNQ48Yep2crakneQ==', CAST(N'2021-11-10T16:16:13.000' AS DateTime), N'LOGIN', 3, N'0564E6194F08BFDFFEEDECAE41A91C44')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (8, 2, N'4TXB7mxNQ48Yep2crakneQ==', CAST(N'2021-11-10T16:16:57.000' AS DateTime), N'LOGIN', 3, N'E8ECB17DCC1ED4BD2108FDA212A22743')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (9, 2, N'4TXB7mxNQ48Yep2crakneQ==', CAST(N'2021-11-10T16:17:45.000' AS DateTime), N'LOGIN', 3, N'75A23C38B0EF5C83012A9C6F3ABCC674')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (10, 2, N'4TXB7mxNQ48Yep2crakneQ==', CAST(N'2021-11-10T17:47:36.000' AS DateTime), N'LOGIN', 3, N'E88B8C1041CB6255CC88A6994DE0E6A4')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (11, 2, N'4TXB7mxNQ48Yep2crakneQ==', CAST(N'2021-11-11T14:49:48.000' AS DateTime), N'LOGIN', 3, N'4435802976AD4F39EBB8B33B3801932E')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (12, 2, N'4TXB7mxNQ48Yep2crakneQ==', CAST(N'2021-11-11T15:19:07.000' AS DateTime), N'LOGIN', 3, N'E377685A1ED8B05B64F3B6528341CE2F')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (13, 2, N'4TXB7mxNQ48Yep2crakneQ==', CAST(N'2021-11-11T15:21:18.000' AS DateTime), N'LOGIN', 3, N'ACD7B532C07E3F777BE4C16DBDBAF342')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (14, 2, N'4TXB7mxNQ48Yep2crakneQ==', CAST(N'2021-11-11T15:28:21.000' AS DateTime), N'LOGIN', 3, N'7763F3C821926F71C7BECABC368CD963')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (15, 2, N'4TXB7mxNQ48Yep2crakneQ==', CAST(N'2021-11-11T15:31:56.000' AS DateTime), N'LOGIN', 3, N'3A9DE8B1861990EECF9120362D62E54C')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (16, 2, N'4TXB7mxNQ48Yep2crakneQ==', CAST(N'2021-11-11T16:15:33.000' AS DateTime), N'LOGIN', 3, N'417AD3D74EECD3CBE8ACEE333C6B5E9D')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (17, 2, N'4TXB7mxNQ48Yep2crakneQ==', CAST(N'2021-11-11T16:49:14.000' AS DateTime), N'LOGIN', 3, N'887114763805AE4FA90731003758A3A2')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (18, 2, N'4TXB7mxNQ48Yep2crakneQ==', CAST(N'2021-11-11T16:51:56.000' AS DateTime), N'LOGIN', 3, N'ED8FEE567DA5FFD3186C28DBF231083D')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (19, 2, N'4TXB7mxNQ48Yep2crakneQ==', CAST(N'2021-11-11T16:57:10.000' AS DateTime), N'LOGIN', 3, N'9F6D6B8A7B9B54DFFD7A87ADFC7E0750')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (20, 2, N'4TXB7mxNQ48Yep2crakneQ==', CAST(N'2021-11-11T16:58:29.000' AS DateTime), N'LOGIN', 3, N'36671AE8569FB31A151595912E4B670F')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (21, 2, N'4TXB7mxNQ48Yep2crakneQ==', CAST(N'2021-11-11T17:01:53.000' AS DateTime), N'LOGIN', 3, N'7B64B8E4169EA41CE10C69BF41CCAF23')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (22, 2, N'4TXB7mxNQ48Yep2crakneQ==', CAST(N'2021-11-11T17:09:58.000' AS DateTime), N'LOGIN', 3, N'E03208A7732C9DDDEE095E6D5B5CEFE4')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (23, 2, N'4TXB7mxNQ48Yep2crakneQ==', CAST(N'2021-11-11T17:41:53.000' AS DateTime), N'LOGIN', 3, N'C83C027F817B0E3CCADA259CF99C566F')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (24, 2, N'4TXB7mxNQ48Yep2crakneQ==', CAST(N'2021-11-11T17:45:10.000' AS DateTime), N'LOGIN', 3, N'BA021ACF6A15CE20B41A53EC33F9169A')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (25, 2, N'4TXB7mxNQ48Yep2crakneQ==', CAST(N'2021-11-11T18:02:16.000' AS DateTime), N'LOGIN', 3, N'A2BEA74D790E9C0CF9DA2D35B5AF097A')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (26, 2, N'4TXB7mxNQ48Yep2crakneQ==', CAST(N'2021-11-11T23:04:38.000' AS DateTime), N'LOGIN', 3, N'34A1C17BBD7CF769FE93BAD24452204D')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (27, 2, N'4TXB7mxNQ48Yep2crakneQ==', CAST(N'2021-11-11T23:05:38.000' AS DateTime), N'LOGIN', 3, N'82F874F68E32797B2B600B5132CBCCEE')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (28, 2, N'4TXB7mxNQ48Yep2crakneQ==', CAST(N'2021-11-11T23:11:47.000' AS DateTime), N'LOGIN', 3, N'2798B4340AA7B59BF49350747A86E465')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (1002, 1, N'V6ZrRmn7NeoCPICVmTItuaEIG8LyK8enqswiGklyp4fi5wZqNJkj9fbyrHsTKoHIRGsn+t4fYObajGV9xsZgdyDbHYSfRsM5u5vFuvpt96+2tvufvZYrveENxW2ff/pFIb22nHNQpGfyymWFiJj6LfD2VADf2dNjvrGEZdFTB4Io2TnFV1h78wVag8FOv+LAX6MU735fGIhVZ+wEr54WRcoKTVWgESawoRx8JZ6pRPk=', CAST(N'2021-06-26T21:37:02.000' AS DateTime), N'INTEGRIDAD DE BASE DATOS', NULL, N'68D66CEF52F4916CB000C465B571E7AF')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (1003, 1, N'V6ZrRmn7NeoCPICVmTItuaEIG8LyK8enqswiGklyp4fi5wZqNJkj9fbyrHsTKoHI0CKS2DGolSxL7hK0PyB3WdLoC78ke/f8B8+i9/0ilRhI8L/9y7F5wQUXVfcQ1f+bhampuiCI568XmuAmJHs1NS7HtbFspkL8sCTYFuNcatClhGNc5pMpYlKa6H61njzF', CAST(N'2021-06-26T21:37:02.000' AS DateTime), N'INTEGRIDAD DE BASE DATOS', NULL, N'CBEB380A43E550E0CB9363F609B6B927')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (1004, 1, N'V6ZrRmn7NeoCPICVmTItuaEIG8LyK8enqswiGklyp4fi5wZqNJkj9fbyrHsTKoHIksnz2CKoE4YX3SxLbaekyyoDoVjwH/BKEH/gaJRhP1gbhA0K6LiXufAqGrBnCtYCmtWIOnohscdL7xt99NZcDq0kif6IuZFPbjGSDMzcb7o=', CAST(N'2021-06-26T21:37:02.000' AS DateTime), N'INTEGRIDAD DE BASE DATOS', NULL, N'B97DB6F2E922A684418AB8EAB3021E44')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (2002, 2, N'9Y4qHG+rs3fIDefHmrmPrs9kYA8GIL42jKSdKh4J9Hg=', CAST(N'2021-06-27T13:28:47.000' AS DateTime), N'ADMINISTRACION DE USUARIOS', 3, N'2B04D48ADE57AF7AFF9C8256DDB4FAB2')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (2003, 2, N'9Y4qHG+rs3fIDefHmrmPrs9kYA8GIL42jKSdKh4J9Hg=', CAST(N'2021-06-27T13:30:10.000' AS DateTime), N'ADMINISTRACION DE USUARIOS', 3, N'A99346A856D13E39D0CEE7EC33B8861F')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (2004, 2, N'9Y4qHG+rs3fIDefHmrmPrs9kYA8GIL42jKSdKh4J9Hg=', CAST(N'2021-06-27T13:31:59.000' AS DateTime), N'ADMINISTRACION DE USUARIOS', 3, N'274C9FC8BB757D5538CBCEA3E511187C')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (2005, 2, N'9Y4qHG+rs3fIDefHmrmPrhQ9RRlRVaCn43BySFXhjLk=', CAST(N'2021-06-27T13:31:59.000' AS DateTime), N'ADMINISTRACION DE USUARIOS', 4, N'051AB572FA713DEF3FF880A6C67B8DEA')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (2006, 2, N'9Y4qHG+rs3fIDefHmrmPrlDkrUGUdsMBhQOIgGu0NVQ=', CAST(N'2021-06-27T13:31:59.000' AS DateTime), N'ADMINISTRACION DE USUARIOS', 5, N'4AC3CC7DDD5CAD79BE36DDE5DFFCD0E0')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (2007, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-06-27T13:32:42.000' AS DateTime), N'LOGIN', 3, N'8C6008A8537A26C8CD9D6BFA97DE4D51')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (2008, 1, N'V6ZrRmn7NeoCPICVmTItuaEIG8LyK8enqswiGklyp4fi5wZqNJkj9fbyrHsTKoHIRGsn+t4fYObajGV9xsZgdyDbHYSfRsM5u5vFuvpt96+2tvufvZYrveENxW2ff/pFIb22nHNQpGfyymWFiJj6LfD2VADf2dNjvrGEZdFTB4Io2TnFV1h78wVag8FOv+LAX6MU735fGIhVZ+wEr54WRcoKTVWgESawoRx8JZ6pRPk=', CAST(N'2021-06-27T13:32:42.000' AS DateTime), N'INTEGRIDAD DE BASE DATOS', NULL, N'5CE5662EA745E5FF077804DCBE054423')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (2009, 1, N'V6ZrRmn7NeoCPICVmTItuaEIG8LyK8enqswiGklyp4fi5wZqNJkj9fbyrHsTKoHI0CKS2DGolSxL7hK0PyB3WdLoC78ke/f8B8+i9/0ilRhI8L/9y7F5wQUXVfcQ1f+bhampuiCI568XmuAmJHs1NS7HtbFspkL8sCTYFuNcatClhGNc5pMpYlKa6H61njzF', CAST(N'2021-06-27T13:32:42.000' AS DateTime), N'INTEGRIDAD DE BASE DATOS', NULL, N'827ACDDAACC1DC01EBA49F032FE3306C')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (2010, 1, N'V6ZrRmn7NeoCPICVmTItuaEIG8LyK8enqswiGklyp4fi5wZqNJkj9fbyrHsTKoHIksnz2CKoE4YX3SxLbaekyyoDoVjwH/BKEH/gaJRhP1gbhA0K6LiXufAqGrBnCtYCmtWIOnohscdL7xt99NZcDq0kif6IuZFPbjGSDMzcb7o=', CAST(N'2021-06-27T13:32:42.000' AS DateTime), N'INTEGRIDAD DE BASE DATOS', NULL, N'50B3F818A38212A2E6B22191954B4819')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (2011, 1, N'BAUhB7dszogmmEjT1becOnjSzKsnr5i1BmqNhxm0lLwEbl2zSVLX7HLHys6oBHjb', CAST(N'2021-06-27T13:32:49.000' AS DateTime), N'INTEGRIDAD DE BASE DATOS', NULL, N'7048340E5E18AB41580A25EA3515B915')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (2012, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-06-27T18:12:06.000' AS DateTime), N'LOGIN', 3, N'AC64A1A148A2537A5E95A73186A00E3E')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (2013, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-06-27T18:19:18.000' AS DateTime), N'LOGIN', 3, N'BF23EFFDA8B288F3B3FBE4AD14ACE9DA')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (2014, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-06-27T18:21:43.000' AS DateTime), N'LOGIN', 3, N'9D1F2B4DBA396C38814F9EE31B43BE51')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (2015, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-06-27T18:27:17.000' AS DateTime), N'LOGIN', 3, N'C8749310100007D117A16F0AD67C31F0')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (2016, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-06-27T18:27:48.000' AS DateTime), N'LOGIN', 3, N'637A7AAA1CBA5FB80523BE7F4BFEDAB8')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (2017, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-06-27T18:28:59.000' AS DateTime), N'LOGIN', 3, N'EDC953788EF7F9EDCB0AD51A12EF0DE3')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (2018, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-06-27T18:42:06.000' AS DateTime), N'LOGIN', 3, N'A40A6DCFA646DE4061D270826ACB5748')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (2019, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-06-27T18:44:02.000' AS DateTime), N'LOGIN', 3, N'4A7EEA63414D8EB1AB72085F65ACC61E')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (2020, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-06-27T18:56:41.000' AS DateTime), N'LOGIN', 3, N'A8870189E8E172A243F568655A3FD127')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (2021, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-06-27T18:58:24.000' AS DateTime), N'LOGIN', 3, N'F9C6DD4842DEF3E18100E20FB0685DE6')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (2022, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-06-27T18:59:57.000' AS DateTime), N'LOGIN', 3, N'3B78895CFD240BB843561C58C031BA2C')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (2023, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-06-27T19:00:25.000' AS DateTime), N'LOGIN', 3, N'668066A7108A2A46F14D32DDA510B1D5')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (2024, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-06-27T19:06:05.000' AS DateTime), N'LOGIN', 3, N'59092CDDC0FE4257220D10B8BC127085')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (2025, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-06-27T22:17:12.000' AS DateTime), N'LOGIN', 3, N'D5CA8D93F408772E0EBB192322AB9604')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (2026, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-06-27T22:28:56.000' AS DateTime), N'LOGIN', 3, N'D8C33240EDC31F9F5078144A4C10F3D3')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (2027, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-06-27T22:30:23.000' AS DateTime), N'LOGIN', 3, N'9F7D5CCF26C77E7EFA80A07DE317F995')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (2028, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-06-27T22:32:19.000' AS DateTime), N'LOGIN', 3, N'3F26BE3B25AE2EB6BB009C8A3DB04EE1')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (2029, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-06-27T22:38:28.000' AS DateTime), N'LOGIN', 3, N'E829639E907ECC545A160F525D3DE628')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (2030, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-06-27T22:42:46.000' AS DateTime), N'LOGIN', 3, N'AD9773FE21B1D6BB0D9A4272EEC4C377')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (3002, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-07-10T18:37:31.000' AS DateTime), N'LOGIN', 3, N'4EC32629846E520096CBC5AA3E8197AA')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (3003, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-07-10T18:43:21.000' AS DateTime), N'LOGIN', 3, N'937EEB2223B04437E8DDEB5677F8DA0C')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (4004, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-07-11T20:12:22.000' AS DateTime), N'LOGIN', 3, N'60F149C163B398D88110030510391B62')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (4005, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-07-11T20:16:33.000' AS DateTime), N'LOGIN', 3, N'EF4E7E8150DA56FC6B2E182110E62C24')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (4006, 3, N'ZCzeLM/G32+ddKrlEQtvn1Yp0CE9ZOVTlvwXrq+TaFE=', CAST(N'2021-07-11T20:22:40.000' AS DateTime), N'ADMINISTRACION DE USUARIOS', 1003, N'4EA0B7E1E118745873718868B526081C')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (4007, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-07-11T20:24:01.000' AS DateTime), N'LOGIN', 1003, N'51C78751CFEA25FF9A1362777BC5A26C')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (5003, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-07-14T01:00:05.000' AS DateTime), N'LOGIN', 3, N'573CA48B420587DCF245E365EB687399')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (5004, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-07-14T01:04:09.000' AS DateTime), N'LOGIN', 3, N'668C8ED39074B81E49B6619D87AE82EA')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (5005, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-07-14T01:11:35.000' AS DateTime), N'LOGIN', 3, N'455CF26A749530044BF4879A5C55A5F0')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (5006, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-07-14T01:17:27.000' AS DateTime), N'LOGIN', 3, N'96230BFDA464D37B32134362D2110773')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (5007, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-07-14T01:18:27.000' AS DateTime), N'LOGIN', 3, N'6C692C3F561B83324A0682BA347CE0F6')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (5008, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-07-14T01:19:59.000' AS DateTime), N'LOGIN', 1003, N'5995DB363FF30A38D6A2014647DBA76B')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (6004, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ85/zbUxQ=', CAST(N'2021-07-14T20:04:26.000' AS DateTime), N'LOGIN', 3, N'BFA0B0C0FE6FC5A2B6FED1A0E08E1E11')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (6005, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2020-07-14T20:27:51.000' AS DateTime), N'LOGIN', 3, N'9F5BB1DBA73EB588611C328E6B0B1E7C')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (6006, 1, N'BAUhB7dszogmmEjT1becOnjSzKsnr5i1BmqNhxm0lLwEbl2zSVLX7HLHys6oBHjb', CAST(N'2021-07-14T23:15:08.000' AS DateTime), N'INTEGRIDAD DE BASE DATOS', NULL, N'146F111959768C67AEEAE5864183146A')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (6007, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-07-14T23:31:37.000' AS DateTime), N'LOGIN', 3, N'FB99C5D6C0406E7C3D2B7C12D6B2F64E')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (6008, 1, N'V6ZrRmn7NeoCPICVmTItuaEIG8LyK8enqswiGklyp4fi5wZqNJkj9fbyrHsTKoHIRGsn+t4fYObajGV9xsZgdyDbHYSfRsM5u5vFuvpt96+2tvufvZYrveENxW2ff/pFIb22nHNQpGfyymWFiJj6LfD2VADf2dNjvrGEZdFTB4Io2TnFV1h78wVag8FOv+LAX6MU735fGIhVZ+wEr54WRcoKTVWgESawoRx8JZ6pRPk=', CAST(N'2021-07-14T23:31:37.000' AS DateTime), N'INTEGRIDAD DE BASE DATOS', NULL, N'C9D427D2A8CD13198E68A7FE691E866B')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (7006, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-07-15T15:58:30.000' AS DateTime), N'LOGIN', 3, N'3883393784D24EE4A6C2961A9BC9741D')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (7007, 1, N'V6ZrRmn7NeoCPICVmTItuaEIG8LyK8enqswiGklyp4fi5wZqNJkj9fbyrHsTKoHIRGsn+t4fYObajGV9xsZgdyDbHYSfRsM5u5vFuvpt96+2tvufvZYrveENxW2ff/pFIb22nHNQpGfyymWFiJj6LfD2VADf2dNjvrGEZdFTB4Io2TnFV1h78wVag8FOv+LAX6MU735fGIhVZ+wEr54WRcoKTVWgESawoRx8JZ6pRPk=', CAST(N'2021-07-15T15:58:30.000' AS DateTime), N'INTEGRIDAD DE BASE DATOS', NULL, N'8833DF82DDC4B6C9A6F013B04C5BF807')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (7008, 1, N'BAUhB7dszogmmEjT1becOnjSzKsnr5i1BmqNhxm0lLwEbl2zSVLX7HLHys6oBHjb', CAST(N'2021-07-15T15:58:32.000' AS DateTime), N'INTEGRIDAD DE BASE DATOS', NULL, N'D02C8695963878B5E361CAF7B942EBA1')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (7009, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-07-15T16:35:28.000' AS DateTime), N'LOGIN', 3, N'CA4353A84C97E3FA6698FF63308FC77C')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (7010, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-07-15T16:48:02.000' AS DateTime), N'LOGIN', 3, N'479ED88C05B96BDD4E6D4856280C8E5B')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (7011, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-07-15T16:48:40.000' AS DateTime), N'LOGIN', 3, N'A5E27EF7FC1314448199465DBE911A5B')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (7012, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-07-15T16:50:36.000' AS DateTime), N'LOGIN', 3, N'64820B703BB1AF996B62622DE0D42917')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (7013, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-07-15T17:10:58.000' AS DateTime), N'LOGIN', 3, N'3ED8306412C4344D75FC78454833D887')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (7014, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-07-15T18:40:17.000' AS DateTime), N'LOGIN', 3, N'E02D31755D92A8CD78CD79753E10D3D5')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (7015, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-07-17T17:31:38.000' AS DateTime), N'LOGIN', 3, N'CF55AD2511E7069390FE92A40FC7AEFF')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (7016, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-07-17T17:34:43.000' AS DateTime), N'LOGIN', 3, N'CD8F5753198212FBCBA772D68D8DCB71')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (7017, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-07-18T00:39:19.000' AS DateTime), N'LOGIN', 3, N'3ED1B36CDC0F7576BF552CF6B175D54F')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (7018, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-07-18T00:39:46.000' AS DateTime), N'LOGIN', 3, N'51264F3FAD09C4D4CD11FEEF01612060')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (7019, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-07-18T00:41:27.000' AS DateTime), N'LOGIN', 3, N'EE83DF2E855241D9D9B6749971370DD2')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (7020, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-07-18T00:43:21.000' AS DateTime), N'LOGIN', 3, N'6B5D54489F76DF9E16CDD999E7018717')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (7021, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-07-18T00:45:00.000' AS DateTime), N'LOGIN', 1003, N'26DCA76A78700C34318E54B395542406')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (7022, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-07-18T00:45:37.000' AS DateTime), N'LOGIN', 1003, N'23C2D573A8575AF9CC493C334D29B615')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (7023, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-07-18T00:46:21.000' AS DateTime), N'LOGIN', 1003, N'E83CE7F3AC3A94AA9EA2E373031A4855')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (7024, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-07-18T00:49:38.000' AS DateTime), N'LOGIN', 1003, N'D88B0CE80519A42BCDE0CD24E637A703')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (7025, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-07-18T00:51:35.000' AS DateTime), N'LOGIN', 1003, N'EB98C7D922276F0F08E55206C91DAF90')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (7026, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-07-18T00:52:01.000' AS DateTime), N'LOGIN', 3, N'1EEB01A262109E5C032027AB11EB3550')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (7027, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-07-18T00:53:30.000' AS DateTime), N'LOGIN', 3, N'FE0DDD7CB841D02634E0C2F06ED595DB')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (7028, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-07-18T00:54:40.000' AS DateTime), N'LOGIN', 3, N'BEE43B475F99665FD9ACDA9DD39CF7F9')
GO
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (7029, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-07-18T01:30:35.000' AS DateTime), N'LOGIN', 3, N'0C5F9DB54E4B17D53946F86D8B4CBC9B')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (7030, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-07-18T02:54:34.000' AS DateTime), N'LOGIN', 3, N'2235D353C35054A6C7B5431D314238A9')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (8015, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-07-19T23:12:27.000' AS DateTime), N'LOGIN', 3, N'91A65A46A4731390A00C76753C6A7E83')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (8016, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-07-19T23:20:13.000' AS DateTime), N'LOGIN', 3, N'762D38B0AB425ABD2C797AEE533D9585')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (8017, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-07-19T23:20:27.000' AS DateTime), N'LOGIN', 1003, N'901C794DEDE41E96B36EBA35AB1F9CDB')
INSERT [dbo].[bitacora] ([idBitacora], [criticidad], [descripcion], [fecha], [funcionalidad], [Usuario_idUsuario], [digitoVerificadorH]) VALUES (8018, 3, N'gXP0OAEIYK0rMJCzXbFR0RSpt429Z/myyJ8T7/zbUxQ=', CAST(N'2021-07-19T23:28:53.000' AS DateTime), N'LOGIN', 3, N'8061B2C63F453D1780B4C89DE0B387DA')
SET IDENTITY_INSERT [dbo].[bitacora] OFF
GO
INSERT [dbo].[Categorias] ([codcategoria], [descategoria]) VALUES (N'3 ', N'Alimento')
INSERT [dbo].[Categorias] ([codcategoria], [descategoria]) VALUES (N'4 ', N'Juguetes')
GO
INSERT [dbo].[Detalle_venta] ([VEN_Codigo], [DV_Cantidad], [DV_Precio], [DV_SubTotal], [codproducto]) VALUES (N'0013      ', 1, CAST(1500.00 AS Decimal(10, 2)), CAST(1500.00 AS Decimal(10, 2)), N'1   ')
INSERT [dbo].[Detalle_venta] ([VEN_Codigo], [DV_Cantidad], [DV_Precio], [DV_SubTotal], [codproducto]) VALUES (N'0013      ', 1, CAST(1231.00 AS Decimal(10, 2)), CAST(1231.00 AS Decimal(10, 2)), N'3   ')
GO
SET IDENTITY_INSERT [dbo].[digitoverificadorvertical] ON 

INSERT [dbo].[digitoverificadorvertical] ([idDigitoVerificadorVertical], [tabla], [digitoVerificador]) VALUES (1, N'BITACORA', N'195293')
INSERT [dbo].[digitoverificadorvertical] ([idDigitoVerificadorVertical], [tabla], [digitoVerificador]) VALUES (2, N'USUARIO', N'20341')
INSERT [dbo].[digitoverificadorvertical] ([idDigitoVerificadorVertical], [tabla], [digitoVerificador]) VALUES (3, N'familiapatente', N'11205')
INSERT [dbo].[digitoverificadorvertical] ([idDigitoVerificadorVertical], [tabla], [digitoVerificador]) VALUES (4, N'PATENTEUSUARIO', N'A3385F532CA2D9E8CA2FC0B60F053479')
SET IDENTITY_INSERT [dbo].[digitoverificadorvertical] OFF
GO
SET IDENTITY_INSERT [dbo].[familia] ON 

INSERT [dbo].[familia] ([idFamilia], [nombre], [habilitado]) VALUES (1, N'S/SMKX5LO5GGBoiPTVchIg==', 1)
INSERT [dbo].[familia] ([idFamilia], [nombre], [habilitado]) VALUES (2, N'S2EAyzryR4w0w+5YG3diAQ==', 1)
INSERT [dbo].[familia] ([idFamilia], [nombre], [habilitado]) VALUES (3, N'R3l5/+vqp3A+g4mvdpe5RQ==', 1)
SET IDENTITY_INSERT [dbo].[familia] OFF
GO
SET IDENTITY_INSERT [dbo].[familiapatente] ON 

INSERT [dbo].[familiapatente] ([idFamiliaPatente], [Patente_idPatente], [Familia_idFamilia], [digitoVerificadorH]) VALUES (1, 1, 1, N'6512BD43D9CAA6E02C990B0A82652DCA')
INSERT [dbo].[familiapatente] ([idFamiliaPatente], [Patente_idPatente], [Familia_idFamilia], [digitoVerificadorH]) VALUES (2, 2, 2, N'B6D767D2F8ED5D21A44B0E5886680CB9')
INSERT [dbo].[familiapatente] ([idFamiliaPatente], [Patente_idPatente], [Familia_idFamilia], [digitoVerificadorH]) VALUES (3, 3, 3, N'182BE0C5CDCD5072BB1864CDEE4D3D6E')
INSERT [dbo].[familiapatente] ([idFamiliaPatente], [Patente_idPatente], [Familia_idFamilia], [digitoVerificadorH]) VALUES (4, 4, 3, N'17E62166FC8586DFA4D1BC0E1742C08B')
INSERT [dbo].[familiapatente] ([idFamiliaPatente], [Patente_idPatente], [Familia_idFamilia], [digitoVerificadorH]) VALUES (5, 5, 1, N'2838023A778DFAECDC212708F721B788')
INSERT [dbo].[familiapatente] ([idFamiliaPatente], [Patente_idPatente], [Familia_idFamilia], [digitoVerificadorH]) VALUES (6, 6, 1, N'7F39F8317FBDB1988EF4C628EBA02591')
SET IDENTITY_INSERT [dbo].[familiapatente] OFF
GO
SET IDENTITY_INSERT [dbo].[familiausuario] ON 

INSERT [dbo].[familiausuario] ([idFamiliaUsuario], [Familia_idFamilia], [Usuario_idUsuario]) VALUES (1, 1, 3)
INSERT [dbo].[familiausuario] ([idFamiliaUsuario], [Familia_idFamilia], [Usuario_idUsuario]) VALUES (2, 2, 4)
INSERT [dbo].[familiausuario] ([idFamiliaUsuario], [Familia_idFamilia], [Usuario_idUsuario]) VALUES (3, 3, 5)
SET IDENTITY_INSERT [dbo].[familiausuario] OFF
GO
SET IDENTITY_INSERT [dbo].[patente] ON 

INSERT [dbo].[patente] ([idPatente], [nombre]) VALUES (1, N'BITACORA')
INSERT [dbo].[patente] ([idPatente], [nombre]) VALUES (2, N'ADMINISRAR_CURSOS')
INSERT [dbo].[patente] ([idPatente], [nombre]) VALUES (3, N'CONSULTAR_CURSOS')
INSERT [dbo].[patente] ([idPatente], [nombre]) VALUES (4, N'MIS_CURSOS')
INSERT [dbo].[patente] ([idPatente], [nombre]) VALUES (5, N'BACKUP')
INSERT [dbo].[patente] ([idPatente], [nombre]) VALUES (6, N'RESTORE')
SET IDENTITY_INSERT [dbo].[patente] OFF
GO
INSERT [dbo].[Productos] ([codproducto], [desproducto], [codcategoria], [preproducto], [canproducto], [imagen]) VALUES (N'1   ', N'royalcanin_max', N'3 ', CAST(1500.00 AS Decimal(18, 2)), 14, N'royalcanin_max.jpg')
INSERT [dbo].[Productos] ([codproducto], [desproducto], [codcategoria], [preproducto], [canproducto], [imagen]) VALUES (N'2   ', N'bocaditos', N'3 ', CAST(300.00 AS Decimal(18, 2)), 3, N'bocaditos.jpg')
INSERT [dbo].[Productos] ([codproducto], [desproducto], [codcategoria], [preproducto], [canproducto], [imagen]) VALUES (N'3   ', N'Pelotas', N'4 ', CAST(1231.00 AS Decimal(18, 2)), 8, N'pelotas.jpg')
INSERT [dbo].[Productos] ([codproducto], [desproducto], [codcategoria], [preproducto], [canproducto], [imagen]) VALUES (N'4   ', N'oldprince_cordero', N'3 ', CAST(2500.00 AS Decimal(18, 2)), 329, N'oldprince_cordero.jpg')
GO
SET IDENTITY_INSERT [dbo].[usuario] ON 

INSERT [dbo].[usuario] ([idUsuario], [nombreUsuario], [nombre], [apellido], [email], [contrasena], [habilitado], [cantidadDeIntentos], [digitoVerificadorH]) VALUES (3, N'hnkeEBj3FnsD5a/1QJeAvQ==', N'admin', N'admin', N'admin@mail.com', N'59397D6112ED13FEAE151A185B36467E', 1, 0, N'B23FB2151B5865C253E65B691CD35DC5')
INSERT [dbo].[usuario] ([idUsuario], [nombreUsuario], [nombre], [apellido], [email], [contrasena], [habilitado], [cantidadDeIntentos], [digitoVerificadorH]) VALUES (4, N'm3DxqP3rImzVRT5P4u0uwQ==', N'Facundo', N'Vazquez', N'f@mail.com', N'59397D6112ED13FEAE151A185B36467E', 1, 0, N'E54D406FA8974CEF1E90BE770DCCCC02')
INSERT [dbo].[usuario] ([idUsuario], [nombreUsuario], [nombre], [apellido], [email], [contrasena], [habilitado], [cantidadDeIntentos], [digitoVerificadorH]) VALUES (5, N'buavlXCsgm+rIlJd6EKzXA==', N'Carlos', N'Sanchez', N'c@mail.com', N'59397D6112ED13FEAE151A185B36467E', 1, 0, N'279AC2CE908EA3D785ED40BF36B28485')
INSERT [dbo].[usuario] ([idUsuario], [nombreUsuario], [nombre], [apellido], [email], [contrasena], [habilitado], [cantidadDeIntentos], [digitoVerificadorH]) VALUES (6, N'30xiTR9zcAtlPIzL2pq2UQ==', N'Velo', N'Martelli', N'v@mail.com', N'1A1DC91C907325C69271DDF0C944BC72', 1, 0, N'E53D067F0623AF0857342DD600C4332B')
INSERT [dbo].[usuario] ([idUsuario], [nombreUsuario], [nombre], [apellido], [email], [contrasena], [habilitado], [cantidadDeIntentos], [digitoVerificadorH]) VALUES (7, N'RhPKr8BvAm5DOlTaVygreg==', N'Braian', N'Romero', N'b@mail.com', N'1A1DC91C907325C69271DDF0C944BC72', 1, 0, N'7CC9F2036B618F31ECAC2B47FF521C9F')
INSERT [dbo].[usuario] ([idUsuario], [nombreUsuario], [nombre], [apellido], [email], [contrasena], [habilitado], [cantidadDeIntentos], [digitoVerificadorH]) VALUES (8, N'mm16CwFRejIYVgC98UfdaA==', N'Maxi', N'Meza', N'm@mail.com', N'1A1DC91C907325C69271DDF0C944BC72', 1, 0, N'3ABE310A75A8395C926CD84C22E81467')
INSERT [dbo].[usuario] ([idUsuario], [nombreUsuario], [nombre], [apellido], [email], [contrasena], [habilitado], [cantidadDeIntentos], [digitoVerificadorH]) VALUES (9, N'HoyPzvXxk+B/a4+PaLak0Q==', N'Alan', N'Franco', N'a@mail.com', N'1A1DC91C907325C69271DDF0C944BC72', 1, 0, N'297417647F90992AA31CB9928F23BC28')
INSERT [dbo].[usuario] ([idUsuario], [nombreUsuario], [nombre], [apellido], [email], [contrasena], [habilitado], [cantidadDeIntentos], [digitoVerificadorH]) VALUES (10, N'VIRCmBC1P50HIjTuQoWU8A==', N'Silvio', N'Romero', N's@mail.com', N'1A1DC91C907325C69271DDF0C944BC72', 1, 0, N'75E6C7CA4CCA5D367FD6A656BAA1A231')
INSERT [dbo].[usuario] ([idUsuario], [nombreUsuario], [nombre], [apellido], [email], [contrasena], [habilitado], [cantidadDeIntentos], [digitoVerificadorH]) VALUES (11, N'Y1kGcKzUF83vIrs9TfW7aQ==', N'Carlos', N'Tevez', N'ct@mail.com', N'1A1DC91C907325C69271DDF0C944BC72', 1, 0, N'D19CEA17743D10D23238A3CB29CD616A')
INSERT [dbo].[usuario] ([idUsuario], [nombreUsuario], [nombre], [apellido], [email], [contrasena], [habilitado], [cantidadDeIntentos], [digitoVerificadorH]) VALUES (12, N'hYo0IrNtZNkJzLuLKtcxtw==', N'Dario', N'Benedeto', N'd@mail.com', N'1A1DC91C907325C69271DDF0C944BC72', 1, 0, N'58560A34F71F495A388FFC059CB64E84')
INSERT [dbo].[usuario] ([idUsuario], [nombreUsuario], [nombre], [apellido], [email], [contrasena], [habilitado], [cantidadDeIntentos], [digitoVerificadorH]) VALUES (1003, N'TfxZuljTQXP4V/JnaIwlcQ==', N'Jose', N'Cliente', N'cliente1@clarin.com', N'59397D6112ED13FEAE151A185B36467E', 1, 0, N'EF617B3142104B0D84EF2F2415F986F1')
SET IDENTITY_INSERT [dbo].[usuario] OFF
GO
INSERT [dbo].[Venta] ([VEN_Codigo], [VEN_Fecha], [VEN_SuTotal], [VEN_IGV], [VEN_Total], [VEN_Cliente]) VALUES (N'0001      ', N'17/07/2014', CAST(3280.00 AS Decimal(10, 2)), CAST(720.00 AS Decimal(10, 2)), CAST(4000.00 AS Decimal(10, 2)), N'ERIKA FLORES ORTIZ')
INSERT [dbo].[Venta] ([VEN_Codigo], [VEN_Fecha], [VEN_SuTotal], [VEN_IGV], [VEN_Total], [VEN_Cliente]) VALUES (N'0002      ', N'01/04/2019', CAST(3280.00 AS Decimal(10, 2)), CAST(720.00 AS Decimal(10, 2)), CAST(4000.00 AS Decimal(10, 2)), N'erika')
INSERT [dbo].[Venta] ([VEN_Codigo], [VEN_Fecha], [VEN_SuTotal], [VEN_IGV], [VEN_Total], [VEN_Cliente]) VALUES (N'0003      ', N'01/04/2019', CAST(3280.00 AS Decimal(10, 2)), CAST(720.00 AS Decimal(10, 2)), CAST(4000.00 AS Decimal(10, 2)), N'erika')
INSERT [dbo].[Venta] ([VEN_Codigo], [VEN_Fecha], [VEN_SuTotal], [VEN_IGV], [VEN_Total], [VEN_Cliente]) VALUES (N'0004      ', N'01/04/2019', CAST(3280.00 AS Decimal(10, 2)), CAST(720.00 AS Decimal(10, 2)), CAST(4000.00 AS Decimal(10, 2)), N'erika')
INSERT [dbo].[Venta] ([VEN_Codigo], [VEN_Fecha], [VEN_SuTotal], [VEN_IGV], [VEN_Total], [VEN_Cliente]) VALUES (N'0005      ', N'04/06/2019', CAST(3280.00 AS Decimal(10, 2)), CAST(720.00 AS Decimal(10, 2)), CAST(4000.00 AS Decimal(10, 2)), N'erika')
INSERT [dbo].[Venta] ([VEN_Codigo], [VEN_Fecha], [VEN_SuTotal], [VEN_IGV], [VEN_Total], [VEN_Cliente]) VALUES (N'0006      ', N'12/7/2021 ', CAST(410.00 AS Decimal(10, 2)), CAST(90.00 AS Decimal(10, 2)), CAST(500.00 AS Decimal(10, 2)), N'qqwq')
INSERT [dbo].[Venta] ([VEN_Codigo], [VEN_Fecha], [VEN_SuTotal], [VEN_IGV], [VEN_Total], [VEN_Cliente]) VALUES (N'0007      ', N'15/7/2021 ', CAST(3160.00 AS Decimal(10, 2)), CAST(840.00 AS Decimal(10, 2)), CAST(4000.00 AS Decimal(10, 2)), N'sebas')
INSERT [dbo].[Venta] ([VEN_Codigo], [VEN_Fecha], [VEN_SuTotal], [VEN_IGV], [VEN_Total], [VEN_Cliente]) VALUES (N'0008      ', N'15/7/2021 ', CAST(1975.00 AS Decimal(10, 2)), CAST(525.00 AS Decimal(10, 2)), CAST(2500.00 AS Decimal(10, 2)), N'asd')
INSERT [dbo].[Venta] ([VEN_Codigo], [VEN_Fecha], [VEN_SuTotal], [VEN_IGV], [VEN_Total], [VEN_Cliente]) VALUES (N'0009      ', N'15/7/2021 ', CAST(1975.00 AS Decimal(10, 2)), CAST(525.00 AS Decimal(10, 2)), CAST(2500.00 AS Decimal(10, 2)), N'd')
INSERT [dbo].[Venta] ([VEN_Codigo], [VEN_Fecha], [VEN_SuTotal], [VEN_IGV], [VEN_Total], [VEN_Cliente]) VALUES (N'0010      ', N'15/7/2021 ', CAST(1975.00 AS Decimal(10, 2)), CAST(525.00 AS Decimal(10, 2)), CAST(2500.00 AS Decimal(10, 2)), N'qqwq')
INSERT [dbo].[Venta] ([VEN_Codigo], [VEN_Fecha], [VEN_SuTotal], [VEN_IGV], [VEN_Total], [VEN_Cliente]) VALUES (N'0011      ', N'15/7/2021 ', CAST(15.80 AS Decimal(10, 2)), CAST(4.20 AS Decimal(10, 2)), CAST(20.00 AS Decimal(10, 2)), N'qqwq')
INSERT [dbo].[Venta] ([VEN_Codigo], [VEN_Fecha], [VEN_SuTotal], [VEN_IGV], [VEN_Total], [VEN_Cliente]) VALUES (N'0012      ', N'15/7/2021 ', CAST(15.80 AS Decimal(10, 2)), CAST(4.20 AS Decimal(10, 2)), CAST(20.00 AS Decimal(10, 2)), N'qqwq')
INSERT [dbo].[Venta] ([VEN_Codigo], [VEN_Fecha], [VEN_SuTotal], [VEN_IGV], [VEN_Total], [VEN_Cliente]) VALUES (N'0013      ', N'15/7/2021 ', CAST(2157.49 AS Decimal(10, 2)), CAST(573.51 AS Decimal(10, 2)), CAST(2731.00 AS Decimal(10, 2)), N'asd')
GO
/****** Object:  Index [fk_Bitacora_Usuario1_idx]    Script Date: 19/9/2021 00:14:14 ******/
CREATE NONCLUSTERED INDEX [fk_Bitacora_Usuario1_idx] ON [dbo].[bitacora]
(
	[Usuario_idUsuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [fk_FamiliaPatente_Familia1_idx]    Script Date: 19/9/2021 00:14:14 ******/
CREATE NONCLUSTERED INDEX [fk_FamiliaPatente_Familia1_idx] ON [dbo].[familiapatente]
(
	[Familia_idFamilia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [fk_FamiliaPatente_Patente_idx]    Script Date: 19/9/2021 00:14:14 ******/
CREATE NONCLUSTERED INDEX [fk_FamiliaPatente_Patente_idx] ON [dbo].[familiapatente]
(
	[Patente_idPatente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [fk_FamiliaUsuario_Familia1_idx]    Script Date: 19/9/2021 00:14:14 ******/
CREATE NONCLUSTERED INDEX [fk_FamiliaUsuario_Familia1_idx] ON [dbo].[familiausuario]
(
	[Familia_idFamilia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [fk_FamiliaUsuario_Usuario1_idx]    Script Date: 19/9/2021 00:14:14 ******/
CREATE NONCLUSTERED INDEX [fk_FamiliaUsuario_Usuario1_idx] ON [dbo].[familiausuario]
(
	[Usuario_idUsuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [fk_PatenteUsuario_Patente1_idx]    Script Date: 19/9/2021 00:14:14 ******/
CREATE NONCLUSTERED INDEX [fk_PatenteUsuario_Patente1_idx] ON [dbo].[patenteusuario]
(
	[Patente_idPatente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [fk_PatenteUsuario_Usuario1_idx]    Script Date: 19/9/2021 00:14:14 ******/
CREATE NONCLUSTERED INDEX [fk_PatenteUsuario_Usuario1_idx] ON [dbo].[patenteusuario]
(
	[Usuario_idUsuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[usuario] ADD  DEFAULT ((0)) FOR [cantidadDeIntentos]
GO
ALTER TABLE [dbo].[bitacora]  WITH CHECK ADD  CONSTRAINT [bitacora$fk_Bitacora_Usuario1] FOREIGN KEY([Usuario_idUsuario])
REFERENCES [dbo].[usuario] ([idUsuario])
GO
ALTER TABLE [dbo].[bitacora] CHECK CONSTRAINT [bitacora$fk_Bitacora_Usuario1]
GO
ALTER TABLE [dbo].[Detalle_venta]  WITH CHECK ADD  CONSTRAINT [FK_DETALLE_VENTA_Productos] FOREIGN KEY([codproducto])
REFERENCES [dbo].[Productos] ([codproducto])
GO
ALTER TABLE [dbo].[Detalle_venta] CHECK CONSTRAINT [FK_DETALLE_VENTA_Productos]
GO
ALTER TABLE [dbo].[Detalle_venta]  WITH CHECK ADD  CONSTRAINT [FK_DETALLE_VENTA_VENTA] FOREIGN KEY([VEN_Codigo])
REFERENCES [dbo].[Venta] ([VEN_Codigo])
GO
ALTER TABLE [dbo].[Detalle_venta] CHECK CONSTRAINT [FK_DETALLE_VENTA_VENTA]
GO
ALTER TABLE [dbo].[familiapatente]  WITH CHECK ADD  CONSTRAINT [familiapatente$fk_FamiliaPatente_Familia1] FOREIGN KEY([Familia_idFamilia])
REFERENCES [dbo].[familia] ([idFamilia])
GO
ALTER TABLE [dbo].[familiapatente] CHECK CONSTRAINT [familiapatente$fk_FamiliaPatente_Familia1]
GO
ALTER TABLE [dbo].[familiapatente]  WITH CHECK ADD  CONSTRAINT [familiapatente$fk_FamiliaPatente_Patente] FOREIGN KEY([Patente_idPatente])
REFERENCES [dbo].[patente] ([idPatente])
GO
ALTER TABLE [dbo].[familiapatente] CHECK CONSTRAINT [familiapatente$fk_FamiliaPatente_Patente]
GO
ALTER TABLE [dbo].[familiausuario]  WITH CHECK ADD  CONSTRAINT [familiausuario$fk_FamiliaUsuario_Familia1] FOREIGN KEY([Familia_idFamilia])
REFERENCES [dbo].[familia] ([idFamilia])
GO
ALTER TABLE [dbo].[familiausuario] CHECK CONSTRAINT [familiausuario$fk_FamiliaUsuario_Familia1]
GO
ALTER TABLE [dbo].[familiausuario]  WITH CHECK ADD  CONSTRAINT [familiausuario$fk_FamiliaUsuario_Usuario1] FOREIGN KEY([Usuario_idUsuario])
REFERENCES [dbo].[usuario] ([idUsuario])
GO
ALTER TABLE [dbo].[familiausuario] CHECK CONSTRAINT [familiausuario$fk_FamiliaUsuario_Usuario1]
GO
ALTER TABLE [dbo].[patenteusuario]  WITH CHECK ADD  CONSTRAINT [patenteusuario$fk_PatenteUsuario_Patente1] FOREIGN KEY([Patente_idPatente])
REFERENCES [dbo].[patente] ([idPatente])
GO
ALTER TABLE [dbo].[patenteusuario] CHECK CONSTRAINT [patenteusuario$fk_PatenteUsuario_Patente1]
GO
ALTER TABLE [dbo].[patenteusuario]  WITH CHECK ADD  CONSTRAINT [patenteusuario$fk_PatenteUsuario_Usuario1] FOREIGN KEY([Usuario_idUsuario])
REFERENCES [dbo].[usuario] ([idUsuario])
GO
ALTER TABLE [dbo].[patenteusuario] CHECK CONSTRAINT [patenteusuario$fk_PatenteUsuario_Usuario1]
GO
ALTER TABLE [dbo].[Productos]  WITH CHECK ADD  CONSTRAINT [FK_Productos_Categorias] FOREIGN KEY([codcategoria])
REFERENCES [dbo].[Categorias] ([codcategoria])
GO
ALTER TABLE [dbo].[Productos] CHECK CONSTRAINT [FK_Productos_Categorias]
GO
ALTER TABLE [dbo].[usuariocurso]  WITH CHECK ADD  CONSTRAINT [FK_usuariocurso_usuario] FOREIGN KEY([Usuario_idUsuario])
REFERENCES [dbo].[usuario] ([idUsuario])
GO
ALTER TABLE [dbo].[usuariocurso] CHECK CONSTRAINT [FK_usuariocurso_usuario]
GO
/****** Object:  StoredProcedure [dbo].[pa_Categoria_actualizar]    Script Date: 19/9/2021 00:14:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[pa_Categoria_actualizar]
@codcategoria char(2),
@descategoria varchar(20)
as
update Categorias set descategoria=@descategoria
where codcategoria=@codcategoria

GO
/****** Object:  StoredProcedure [dbo].[pa_Categoria_insertar]    Script Date: 19/9/2021 00:14:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[pa_Categoria_insertar]
@codcategoria char(2),
@descategoria varchar(20)
as
insert into Categorias(codcategoria, descategoria)
values (@codcategoria,@descategoria)

GO
/****** Object:  StoredProcedure [dbo].[pa_Categorias_ListasTodos]    Script Date: 19/9/2021 00:14:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[pa_Categorias_ListasTodos]
as 
select codcategoria,descategoria from Categorias

GO
/****** Object:  StoredProcedure [dbo].[pa_detalleventa]    Script Date: 19/9/2021 00:14:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[pa_detalleventa]
@codigo char(10),
@cantidad int,
@precio decimal(10,2),
@subtotal decimal(10,2),
@codproducto char(4)
as
insert into Detalle_venta(VEN_Codigo, DV_Cantidad, DV_Precio, DV_SubTotal, codproducto)
values (@codigo,@cantidad,@precio,@subtotal,@codproducto)


GO
/****** Object:  StoredProcedure [dbo].[pa_Productos_actualizar]    Script Date: 19/9/2021 00:14:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[pa_Productos_actualizar]
@codproducto char(2),
@desproducto varchar(40),
@codcategoria char(2),
@preproducto decimal(18,2),
@canproducto int,
@imagen varchar(40)
as
update Productos set desproducto=@desproducto,codcategoria=@codcategoria,
					 preproducto=@preproducto,canproducto=@canproducto, imagen=@imagen
where codproducto=@codproducto

GO
/****** Object:  StoredProcedure [dbo].[PA_Productos_Bucar_Por_Categorias]    Script Date: 19/9/2021 00:14:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[PA_Productos_Bucar_Por_Categorias]
@codcategoria char(2)
as
select p.codproducto,p.desproducto,p.codcategoria,p.preproducto,p.canproducto,p.imagen 
from Productos p
where p.codcategoria = @codcategoria

GO
/****** Object:  StoredProcedure [dbo].[pa_Productos_eliminar]    Script Date: 19/9/2021 00:14:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pa_Productos_eliminar]
@codproducto char(4)
AS
BEGIN
    DELETE FROM Productos where Productos.codcategoria=@codproducto
END

GO
/****** Object:  StoredProcedure [dbo].[pa_Productos_insertar]    Script Date: 19/9/2021 00:14:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[pa_Productos_insertar]
@codproducto char(2),
@desproducto varchar(40),
@codcategoria char(2),
@preproducto decimal(18,2),
@canproducto int,
@imagen varchar(40)
as
insert into Productos(codproducto,desproducto,codcategoria,preproducto,canproducto,imagen)
values (@codproducto,@desproducto,@codcategoria,@preproducto,@canproducto,@imagen)


GO
/****** Object:  StoredProcedure [dbo].[pa_productos_ListarTodos]    Script Date: 19/9/2021 00:14:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[pa_productos_ListarTodos]
as
select p.codproducto,p.desproducto,p.codcategoria,p.preproducto,p.canproducto,p.imagen, cat.codcategoria, cat.descategoria
from Productos p inner join Categorias cat on p.codcategoria=cat.codcategoria

GO
/****** Object:  StoredProcedure [dbo].[pa_ventas]    Script Date: 19/9/2021 00:14:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[pa_ventas]
@codigo char(10),
@fecha varchar(50),
@subtotal decimal(10,2),
@igv decimal(10,2),
@total decimal(10,2),
@cliente varchar(50)
as
insert into Venta(VEN_Codigo, VEN_Fecha, VEN_SuTotal, VEN_IGV, VEN_Total, VEN_Cliente)
values (@codigo,@fecha,@subtotal,@igv,@total,@cliente)


GO
USE [master]
GO
ALTER DATABASE [PetshopDB] SET  READ_WRITE 
GO
